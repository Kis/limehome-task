module.exports = (sequelize, type) => {
    return sequelize.define('booking', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        propertyId: type.STRING,
        from: type.DATE,
        to: type.DATE
    });
};
