
const Sequelize = require('sequelize');
const BookingModel = require('../models/booking');
const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        dialect: 'postgres',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT || 5432
    }
);
const Booking = BookingModel(sequelize, Sequelize);
const Models = { Booking };
const connection = {};

module.exports.init = async() => {
    if (connection.isConnected) {
        console.log('=> Using existing connection.');
        return Models;
    }

    await sequelize.sync();
    await sequelize.authenticate();
    connection.isConnected = true;
    console.log('=> Created a new connection.');
    return Models;
};

module.exports.Models = Models;
