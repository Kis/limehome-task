

module.exports = {
    ok,
    badRequest,
    forbidden,
    notFound
};

function ok(data) {
    const statusCode = 200;
    return _response(statusCode, data);
}

function badRequest(data) {
    const statusCode = 400;
    return _response(statusCode, data);
}

function forbidden(data) {
    const statusCode = 200;
    return _response(statusCode, data);
}

function notFound(data) {
    const statusCode = 200;
    return _response(statusCode, data);
}

function _response(statusCode, data) {
    return {
        statusCode,
        body: JSON.stringify(data)
    };
}

