
const joi = require('joi');
const {badRequest} = require('./response');
module.exports.validate = (params, schema) => {
    const result = joi.validate(params, schema);
    console.log(result);
    if (result.error) {
        console.log('joi error', (result.error && result.error.message));
        throw badRequest({error: 'joi validation failed'});
    }
    return result.value;
};
