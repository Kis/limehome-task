module.exports = {
    db: require('./db'),
    response: require('./response'),
    joiValidation: require('./joi')
};
