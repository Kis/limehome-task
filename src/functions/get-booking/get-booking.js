
const response = require('../../util/response');
const connectToDB = require('../../util/db');
const service = require('./get-booking.service');


/**
 * @api {get} /api/properties/:propertyId/bookings Returns the bookings for a property
 * @apiName PropertyBookings
 * @apiGroup Booking
 *
 * @apiParam {string} propertyId Id of property e.g 840dr5ru-f098cb3946ae4be99eeea0181ad7746e
 *
 * @apiSuccessExample {json} Success-Response:
 *       HTTP/1.1 200 OK
 *        [
 *           {
 *               "id": 1,
 *               "propertyId": "11",
 *               "from": "2020-03-02 00:00:00.000 +00:00",
 *               "to": "2020-03-04 00:00:00.000 +00:00",
 *               "createdAt": "2020-02-28T12:45:38.225Z",
 *               "updatedAt": "2020-02-28T12:45:38.225Z"
 *            }
 *        ]
 */


module.exports.handler = async(event) => {
    try {
        const params = event.pathParameters || {};
        await connectToDB.init();
        const result = await service.getBooking(params);
        return response.ok(result);
    } catch (e) {
        if (e.statusCode) {
            return {
                statusCode: e.statusCode,
                body: e.body
            };
        }
        return response.forbidden(e);
    }
};

