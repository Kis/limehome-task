const joi = require('joi');
const joiValidation = require('../../util/joi').validate;
const {Booking} = require('../../util/db').Models;
module.exports.getBooking = async({propertyId}) => {
    try {
        _validation({propertyId});
        return _getBookings(propertyId);
    } catch (e) {
        throw e;
    }
};

function _validation(data) {
    try {
        const schema = joi.object().keys({
            propertyId: joi.string().required()
        });
        joiValidation(data, schema);
    } catch (e) {
        throw e;
    }
}

async function _getBookings(propertyId) {
    try {
        const result = await Booking.findAll({where: {propertyId}});
        return result;
    } catch (e) {
        throw e;
    }
}

