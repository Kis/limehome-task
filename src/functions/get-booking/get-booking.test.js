jest.mock('../../util/db', () => {
    const Models = {
        Booking: {
            findAll: () => {
                return [
                    {
                        'id': 1,
                        'propertyId': '11',
                        'from': '2020-03-02 00:00:00.000 +00:00',
                        'to': '2020-03-04 00:00:00.000 +00:00',
                        'createdAt': '2020-02-28T12:45:38.225Z',
                        'updatedAt': '2020-02-28T12:45:38.225Z'
                    },
                    {
                        'id': 2,
                        'propertyId': '11',
                        'from': '2020-03-02 00:00:00.000 +00:00',
                        'to': '2020-03-04 00:00:00.000 +00:00',
                        'createdAt': '2020-02-28T12:51:47.200Z',
                        'updatedAt': '2020-02-28T12:51:47.200Z'
                    }
                ];
            }
        }
    };
    return {
        init: () => {return Promise.resolve(Models);},
        Models
    };
});
const {handler} = require('./get-booking');

const event = {
    'pathParameters': {
        'propertyId': '11'
    }

};

describe('get booking', () => {
    it('should return booking ', async(done) => {
        const res = await handler(event);
        expect(res.statusCode).toEqual(200);
        expect(JSON.parse(res.body).length).toEqual(2);
        done();
    });
});
