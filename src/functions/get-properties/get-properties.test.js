jest.mock('request-promise');
const {handler} = require('./get-properties');
const fake_here_app_key = '111';
const event = {
    'queryStringParameters': {}
};
describe('get properties', () => {
    describe('FAILED', () => {
        it('should throw error - at parameter is missed ', async(done) => {
            const res = await handler(event);
            expect(res.statusCode).toEqual(400);
            expect(JSON.parse(res.body)).toEqual('query parameter at is missed');
            done();
        });
    });
    describe('Success', () => {
        it('should get properties ', async(done) => {
            event.queryStringParameters.at = '40.74917,-73.98529';
            process.env.HERE_API_KEY = fake_here_app_key;
            const res = await handler(event);
            expect(res.statusCode).toEqual(200);
            expect(JSON.parse(res.body).length).toEqual(2);
            done();
        });
    });
});
