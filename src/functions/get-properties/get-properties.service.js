const joi = require('joi');
const rp = require('request-promise');
const joiValidation = require('../../util/joi').validate;
const {badRequest} = require('../../util/response');
module.exports.getProperties = async({at}) => {
    try {
        _validation(at);
        const uri = await _findPlacesByAt(at);
        if (!uri) return [];
        const result = await _getDetailedPageWithPlaces(uri);
        return result;
    } catch (e) {
        throw e;
    }
};


async function _findPlacesByAt(at) {
    try {
        const options = {
            uri: 'https://places.sit.ls.hereapi.com/places/v1/autosuggest',
            qs: {
                at,
                q: 'hotel',
                cat: 'accommodation',
                apiKey: process.env.HERE_API_KEY
            },
            json: true
        };
        console.log('options', options);
        const result = await rp(options);
        if (result && result.results && result.results[0]) return result.results[0].href;
    } catch (e) {
        throw badRequest(e.error.error_description);
    }
}

async function _getDetailedPageWithPlaces(uri) {
    try {
        const options = {
            uri,
            json: true
        };
        const result = await rp(options);
        return result.results.items.map(({id, title, vicinity}) => ({id, title, vicinity}));
    } catch (e) {
        throw badRequest(e.error.error_description);
    }
}
function _validation(data) {
    const [lat, long] = data.split(',');
    try {
        const schema = joi.object().keys({
            lat: joi.string().required(),
            long: joi.string().required()
        });
        joiValidation({lat, long}, schema);
    } catch (e) {
        throw e;
    }
}


