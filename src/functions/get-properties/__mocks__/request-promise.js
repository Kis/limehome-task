let rp = jest.genMockFromModule('request-promise');
rp = jest.fn().mockImplementation((params) => {
    if (params.qs && params.qs.at && !params.qs.apiKey) {
        return Promise.reject({ error: {error: 'Unauthorized',
            error_description: 'ApiKey invalid. ApiKey not found.' }});
    } else if (!params.qs) {
        return Promise.resolve({
            results: {
                items: [
                    {
                        'id': '840dr5ru-26a6b3d3c37641f4a3cb5ce0c75fa11f',
                        'title': 'Hotel Pennsylvania',
                        'vicinity': '401 7th Ave New York, NY 10001'
                    },
                    {
                        'id': '840dr5ru-8c57599f37c340afa3771d86cdba3408',
                        'title': 'Hampton Inn-Manhattan-35th Street',
                        'vicinity': '59 W 35th St New York, NY 10001'
                    }
                ]
            }
        });
    } else {
        return Promise.resolve({ results:
                [ { title: 'hotel',
                    highlightedTitle: '<b>hotel</b>',
                    category: 'hotel',
                    href:
                        'https://places.sit.ls.hereapi.com/places/v1/autosuggest/search;context=Zmxvdy1pZD1hN2JkNjE1ZC1hN2M0LTVhMWYtODk5NC05NDNkMjdjMDliY2NfMTU4Mjk2MzYzNzI5Nl81NTMwXzUyMjcmcmFuaz0wJmF0PTQwLjc0OTE3JTJDLTczLjk4NTI5?filters=category%3Ahotel%2Cvi%3Aautosuggest&app_id=1cFBiKM82uU1SZVyBgYO&app_code=ianVkpkyPuKTZCS7nXIk6w',
                    type: 'urn:nlp-types:search',
                    resultType: 'category' },
                { title: 'Hotel Indigo',
                    highlightedTitle: '<b>Hotel</b> Indigo',
                    href:
                            'https://places.sit.ls.hereapi.com/places/v1/autosuggest/search;context=Zmxvdy1pZD1hN2JkNjE1ZC1hN2M0LTVhMWYtODk5NC05NDNkMjdjMDliY2NfMTU4Mjk2MzYzNzI5Nl81NTMwXzUyMjcmcmFuaz0xJmF0PTQwLjc0OTE3JTJDLTczLjk4NTI5?filters=vi%3Aautosuggest%2Cchain%3A2545&app_id=1cFBiKM82uU1SZVyBgYO&app_code=ianVkpkyPuKTZCS7nXIk6w',
                    type: 'urn:nlp-types:search',
                    resultType: 'chain' }
                ]
        });
    }
});
module.exports = rp;
