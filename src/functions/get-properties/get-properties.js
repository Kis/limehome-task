const response = require('../../util/response');
const service = require('./get-properties.service');
/**
 * @api {get} /api/properties?at=:at Returns the property around Lat/Lon
 * @apiName ListProperties
 * @apiGroup Booking
 * @apiParam {string} at LAT,LONG e.g 40.74917,-73.98529
 *
 * @apiSuccessExample {json} Success-Response:
 *       HTTP/1.1 200 OK
 *        [
 *            {
 *               "id": "840dr5ru-26a6b3d3c37641f4a3cb5ce0c75fa11f",
 *               "title": "Hotel Pennsylvania",
 *               "vicinity": "401 7th Ave<br/>New York, NY 10001"
 *             },
 *            {
 *               "id": "840dr5ru-8c57599f37c340afa3771d86cdba3408",
 *               "title": "Hampton Inn-Manhattan-35th Street",
 *               "vicinity": "59 W 35th St<br/>New York, NY 10001"
 *             }
 *        ]
 */


module.exports.handler = async(event) => {
    try {
        const queryParams = event.queryStringParameters || {};
        if (!queryParams.at) {
            return response.badRequest('query parameter at is missed');
        }
        const result = await service.getProperties(queryParams);
        return response.ok(result);
    } catch (e) {
        if (e.statusCode) {
            return {
                statusCode: e.statusCode,
                body: e.body
            };
        }
        return response.forbidden(e);
    }
};

