const joi = require('joi');
const joiValidation = require('../../util/joi').validate;
const {Booking} = require('../../util/db').Models;
module.exports.createBooking = async(data) => {
    try {
        _validation(data);
        return _createBooking(data);
    } catch (e) {
        throw e;
    }
};

async function _createBooking(data) {
    try {
        return Booking.create(data);
    } catch (e) {
        throw e;
    }
}

function _validation(data) {
    try {
        const schema = joi.object().keys({
            propertyId: joi.string().required(),
            from: joi.date().iso().required(),
            to: joi.date().iso().greater(joi.ref('from')).required()
        });
        joiValidation(data, schema);
    } catch (e) {
        throw e;
    }
}
