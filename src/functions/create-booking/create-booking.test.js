jest.mock('../../util/db', () => {
    const Models = {
        Booking: {
            create: () => {
                return [
                    {
                        'id': 1,
                        'propertyId': '11',
                        'from': '2020-03-02 00:00:00.000 +00:00',
                        'to': '2020-03-04 00:00:00.000 +00:00',
                        'createdAt': '2020-02-28T12:45:38.225Z',
                        'updatedAt': '2020-02-28T12:45:38.225Z'
                    },
                    {
                        'id': 2,
                        'propertyId': '11',
                        'from': '2020-03-02 00:00:00.000 +00:00',
                        'to': '2020-03-04 00:00:00.000 +00:00',
                        'createdAt': '2020-02-28T12:51:47.200Z',
                        'updatedAt': '2020-02-28T12:51:47.200Z'
                    }
                ];
            }
        }
    };
    return {
        init: () => {return Promise.resolve(Models);},
        Models
    };
});
const {handler} = require('./create-booking');


describe('create booking', () => {
    it('should throw error - missed some of body parameters ', async(done) => {
        const event = {
            'body': '{"propertyId":"11"}'
        };
        const res = await handler(event);
        expect(res.statusCode).toEqual(400);
        expect(JSON.parse(res.body).error).toEqual('joi validation failed');
        done();
    });

    it('should throw error - from param must be less that to param', async(done) => {
        const event = {
            'body': '{"propertyId":"11","from":"2020-03-04","to":"2020-03-02"}'
        };
        const res = await handler(event);
        expect(res.statusCode).toEqual(400);
        expect(JSON.parse(res.body).error).toEqual('joi validation failed');
        done();
    });

    it('should create booking', async(done) => {
        const event = {
            'body': '{"propertyId":"11","from":"2020-03-04","to":"2020-03-06"}'
        };
        const res = await handler(event);
        expect(res.statusCode).toEqual(200);
        done();
    });
});
