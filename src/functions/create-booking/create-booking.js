
const response = require('../../util/response');
const connectToDB = require('../../util/db');
const service = require('./create-booking.service');


/**
 * @api {post} /api/bookings Creates a booking for a property.
 * @apiName CreateBooking
 * @apiGroup Booking
 *
 * @apiParam {string} propertyId Id of property e.g 840dr5ru-f098cb3946ae4be99eeea0181ad7746e
 * @apiParam {string} from Id of property e.g  "2020-03-02"
 * @apiParam {string} to  end date of booking e.g "2020-03-04"

 *
 * @apiSuccessExample {json} Success-Response:
 *       HTTP/1.1 200 OK
 */


module.exports.handler = async(event) => {
    try {
        const params = JSON.parse(event.body) || {};
        await connectToDB.init();
        await service.createBooking(params);
        return response.ok();
    } catch (e) {
        if (e.statusCode) {
            return {
                statusCode: e.statusCode,
                body: e.body
            };
        }
        return response.badRequest(e);
    }
};

