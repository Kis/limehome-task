# Limehome
[![serverless](http://public.serverless.com/badges/v3.svg)](http://www.serverless.com)
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/323d7bd73257ace7275d)

Functions deployed with Serverless framework to [AWS Lambda](https://aws.amazon.com/lambda/) at

[https://4addss7af0.execute-api.eu-west-1.amazonaws.com/production](https://4addss7af0.execute-api.eu-west-1.amazonaws.com/production)

[Postman collection](https://www.getpostman.com/collections/323d7bd73257ace7275d)
can be used to test endpoints locally and remotely.

| function             | type     | path                                      | description                                                                                            |
| -------------------- | -------- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| List-Properties      | http     | GET /api/properties                       | Returns the property around Lat/Lon
| Create-Booking       | http     | POST /api/bookings                        | Creates a booking for a property.                                                                        |
| Property-bookings    | http     | GET /api/properties/{propertyId}/bookings | Returns the bookings for a property

Minimal requirements to set up the project:

- [Node.js](https://nodejs.org/en) v12, installation instructions can be found
  on the official website
- A package manager [npm](https://www.npmjs.com). All instructions in the
  documentation will follow the npm syntax.
- [Serverless](https://serverless.com/)
- [Postgres](https://www.postgresql.org/)
- [Git](https://git-scm.com) client.

# Installing

Start by cloning the repository:

```bash
git clone git@gitlab.com:Kis/limehome-task.git

```
Go the the right directory and install dependencies:

```bash
cd limehome-task
npm install
```

Install serverless:

```bash
npm install -g serverless
```

In order to deploy project, run a command
```bash
sls deploy --stage=production
```

That's it! You can now go to the next step.

# Run locally
Environment is managed with [`dotenv`](https://www.npmjs.com/package/dotenv).
Rename `.env.example` to `.env`.

Instead of 'xxx' please put your value of variables.

Run tests with coverage:

```shell
npm run test:coverage
```

Alternatively `serverless offline` can be used to run functions locally


```shell
sls offline start
```

It is possible to invoke functions locally with `sls invoke local --function <functionName>`

# Documentation
Documentation is managed with npm package - [apidoc](https://apidocjs.com/)

Documentation is generated during CI/CD only after successful deployment step.

Documentation you can find [here](https://kis.gitlab.io/limehome-task/)
# Tests

All tests are being executed using Jest. All tests files live side-to-side with
a source code and have a common suffix: `.test.js`. 
There are three helper scripts to run tests in the most common scenarios:

```bash
npm run test
npm run test:watch
npm run test:coverage
```

# Continuous Integration / Delivery

Gitlab CI/CD are configured and run the **tests**, **linting**, **deployment**  whenever a commit is pushed to this
repository `master` or any other branch.


# Linting

This project uses [ESLint](https://eslint.org) to enable static analysis.
Files are linted using a [custom configuration](./.eslintrc). You can
use one of the following scripts to validate and optionally fix all of the
files:

```bash
npm run lint
npm run lint:fix
```

# Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

# Automation

- [Serverless](https://serverless.com)
